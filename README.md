# API Program Governance

This repository houses governance documentation for UW-Madison's API Program -- the policies, practices, and patterns relating to APIs expressed through the UW API gateway (Apigee).

## About 

 * Who is the intended audience for this content?
 * Who can contribute to this document?
 * How can I contribute (noob to git pro)?
 * Why do we track API Program policies, practices, and patterns this way? (G vs g, democratization)
 * 

## To Do

Since this is an initial commit, putting some placeholder content here to give an idea of the intended structure. "To do" content will of course be kept in [Jira](https://jira.doit.wisc.edu/jira/secure/RapidBoard.jspa?rapidView=12521&view=detail) after this initial commit.

 * Create a table of contents in the main README
 * Create READMEs for each of the policy, practice, pattern directories
 * Explain how to use this documentation, and how it will be expressed in the gateway
 * Explain how to use this process to submit requests and proposed changes
 * Explain how to submit requests/changes if you aren't comfortable or able to use git
 * Recommend an initial governance team consisting at least of EI's IPT and Cybersecurity
 * Describe the decision-making flow and how governance changes will be made
 * Template, naming convention, and format of each decision document
