# Patterns

Patterns are themes, trends, and collections of practices that are used broadly, usually well beyond the scope of UW-Madison. 

Examples might include links and/or documentation about RESTful APIs, technologies like GraphQL and where it might have advantages over a RESTful approach, information about why APIs have moved away from the SOAP pattern, service oriented architecture and its pros/cons, domain driven design, security and authorization technologies like OAuth, etc.