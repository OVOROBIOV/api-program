# API Program Policy Template
Use this template to craft a new policy or to guide you through an existing policy. 

## Policy 

This section should contain only the policy. Any additional background information should be included in the section below. When crafting a policy, usage of the words below should conform to the standards outlined in [RFC 2119](https://tools.ietf.org/html/rfc2119). These words are often capitalized by convention, but this is not a requirement for API Program policies.

 * MUST (NOT)
 * REQUIRED
 * SHALL (NOT)
 * SHOULD (NOT)
 * RECOMMENDED
 * MAY

Sample policies are available in the [/policies](policies) directory and can be used as guidance for new policy creation.

## Background

This section should contain any relevant information that might help clarify the intent, scope, reasoning, or any other detail of the policy. While the policy statements above should be succint and simple to understand, the background section can be used to elaborate and provide context for a policy.

As an example, consider a policy that defines a default quota that limits the number of requests to a proxied API. The background section might include details that address the following questions:

 * How are quotas determined? 
 * Are they per unique consuming application? 
 * When does a quota reset?
 * When is it appropriate to request an exception to the policy?
 * What is the process for requesting an exception?
 * How do you know if you've reached a quota limit?

The above list is certainly not exhaustive, but it gives a sense of the type of content to put in this section.

## Links

This section is for links to related UW-Madison policies and/or relevant documentation. Because the scope of the policies in this repository are contained within the API Program, these are not represented in the [UW-Madison Policy Library](https://policy.wisc.edu/library/). Where appropriate, link to related Policy Library policies or other relevant documentation in this section. 

As an example, consider an API Program policy that requires certain controls to protect APIs that expose restricted data. The [UW-Madison Data Classification Policy](https://policy.wisc.edu/library/UW-504) should be linked in this section.