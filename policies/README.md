# Policies

Policies are rules that are agreed upon by API Program governance and implemented in the API gateway through technical controls. They will be technically enforced in the API Gateway, and will be monitored and enforced. Policies without a technical control will be relegated to practices or will be governed outside of the API Program (e.g. [UW-Madison Policy Library](https://policy.wisc.edu/library/)).

A template that describes each section of the policies can be found [here](/policies/template.md).

## Status

Policies can be in a variety of statuses that reflect where they are in the lifecycle. The statuses are defined below.

| Status | Description |
| --- | --- |
| Proposed | Some lightweight starting point? |
| Development | The policy is still being developed. The policy has not been formally reviewed by the API Program governance team, nor has it been implemented. |
| Proof of Concept | The policy is in a draft mode and is actively being tested. Changes may still be submitted for review and incorporation prior to the policy being ratified. Stakeholders may be invited to test corresponding technical controls in an appropriate test environment. |
| Review | The policy has been submitted to the API Program governance team for review and technical analysis. Upon approval, the policy will be merged into the repository, the appropriate technical controls will be implemented, and stakeholders will be notified. |
| Implemented | The policy has been approved by the API Program governance team and the corresponding technical controls have been implemented. The API gateway will monitor and enforce the policy. Requests fo changes to the policy should be submitted through the the process outlined in the repository [README](/README.md). |

## Policy Register

| ID | Status | Name/Link |
| --- | --- | --- |
| API-1 | Development | [API Framework](/policies/api-1.md) |
