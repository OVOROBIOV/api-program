# Glossary

This document defines key terms used within the API Program.

| Term | Definition |
| --- | --- |
| API Gateway | The platform used to publish, consume, and proxy UW-Madison APIs. The gateway is powered by Google's [Apigee API Management](https://cloud.google.com/apigee) product. |
| API Program | The collection of policies, practices, and patterns that govern UW-Madison's API ecosystem. 
| Policy | Rules for API publishers and consumers, agreed upon by the UW-Madison API community and related governing body, and implemented by DoIT's Integration Platform team as technical controls within the API Gateway. An example of a *policy* |
| Practice | A way of going about something that is not bound by technical controls. An example of a *practice* might include unit testing an API before publishing it.  While this isn't bound by a technical control in the API Gateway |
| Pattern |  |