# Practices

Practices are opinionated ways of doing things to help API publishers and consumers go about the business of creating, publishing, managing, and consuming APIs. These practices don't rise to the level of policies (things that are agreed upon by governance, monitored, and enforced), but are important for publishers to understand and implement.

Consistent practices help to improve the overall user and developer experiences as the ecosystem of APIs grows over time. Publishing and consuming APIs is by nature a community activity, so it is important for stakeholders to adhere to and help update this documentation.