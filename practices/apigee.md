# Apigee

"Apigee is a platform for developing and managing APIs. By fronting services with a proxy layer, Apigee provides an abstraction or facade for your backend service APIs and provides security, rate limiting, quotas, analytics, and more."

https://cloud.google.com/apigee/docs/api-platform/get-started/what-apigee

## Getting Access to Publish APIs

Apigee is meant for teams that want to publish APIs for others to use.
When others browse APIs and get access to use them, they will do so through a Developer Portal instead of interacting with Apigee directly.

Follow these steps to get access to Apigee and publish an API.

1. Contact the Integration Platform Team (integration-platform@doit.wisc.edu) and include your name, the team/department you're representing, and an overview of your use-case for using Apigee. Someone will be in contact with you between 1-3 business days to gather more information or schedule a call to discuss further.
2. After confirming the Integration Platform Team is able to onboard your team to Apigee, [create a Google Group](https://kb.wisc.edu/googleapps/page.php?id=98121) that contains the team members that you would like to allow access to Apigee. Send the name of the group (e.g. my-team@g-groups.wisc.edu) to the Integration Platform Team. This group will be controlled by your team and is used to provide or remove your team's access to Apigee. Note, your team may only consist of yourself, but it is still beneficial to have access controlled through a group in case API ownership needs to be transferred in the future.

## Login to Apigee

Login to Apigee here: https://apigee.google.com/

Apigee uses Google and NetID login for authentication. Make sure you are logged into Google using your NetID.

If prompted, make sure you are using the project `doit-ipt-apigee-dev-808d`

## Onboarding a New API

1. [Create a reverse proxy for your API](https://cloud.google.com/apigee/docs/api-platform/fundamentals/understanding-apis-and-api-proxies)
    * If your API is IP restricted, add 34.71.118.160 to your allow-list
2. [Publish your API](https://cloud.google.com/apigee/docs/api-platform/publish/publishing-overview)


## Understanding the [Framework](https://git.doit.wisc.edu/interop/apigee/framework)

This API Proxy Framework is used to illustrate “Best Practices” through implementing code bundles in Apigee and may serve as a “Jumpstart” for your environment. 

The Framework code bundles are provided as a starting point to get you thinking in terms of “g”overnance by design for your environment and intended to be modified based on your needs. It’s the aggregation of design patterns that enforce best practices.

### Role of Gateway

1. Cross-cutting concerns: Achieved through the use of [Sharedflows](./sharedflows.md)
- Security
- Spike Arrest
- Quota
- Concurrent Rate Limit
- Threat Detection
- Dynamic Routing
- Transformation
- Redaction/Enrichment
- Management/Monitoring (Logging, Analytics)
- Manage Products, Developers and Subscriptions (Apps)

2. Business Logic to a certain extent. The more that business logic bleeds into the gateway, the more tightly coupled your gateway becomes to your api teams. Business logic on the gateway is a sign of an unhealthy API layer. Use if you must, but take the Technical Debt to solve the problem in the API layer

